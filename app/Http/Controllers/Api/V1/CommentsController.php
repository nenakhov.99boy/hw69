<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Comments\CommentStoreRequest;
use App\Http\Resources\ArticleResource;
use App\Http\Resources\CommentResource;
use App\Models\Article;
use App\Models\Comment;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $comment = Comment::paginate(10);
        return CommentResource::collection($comment);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CommentStoreRequest $request)
    {
        if (auth('sanctum')->check()) {
            $comment = Comment::create($request->validated());
            return new CommentResource($comment);
        }
        return 'Action unauthorized';
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        if (auth('sanctum')->check()) {
            $comment = Comment::find($id);
            $comment->with('user')->with('article');
            return new CommentResource($comment);
        }
        return 'Action unauthorized';
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(CommentStoreRequest $request, string $id)
    {
        if (auth('sanctum')->check()) {
            $comment = Comment::find($id);
            $comment->update($request->all());
            return new CommentResource($comment);
        }
        return 'Action unauthorized';
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        if (auth('sanctum')->check()) {
            $comment = Comment::find($id);
            $comment->delete();
            $comment = Comment::paginate(10);
            return CommentResource::collection($comment);
        }
        return 'Action unauthorized';
    }
}
