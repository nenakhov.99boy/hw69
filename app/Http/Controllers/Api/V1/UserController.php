<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Users\UserStoreRequest;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function store(UserStoreRequest $request)
    {
        $validated = $request->validated();
        $user = new User($validated);
        return ['token' => $user->createToken('user_token')->plainTextToken];
    }
}
