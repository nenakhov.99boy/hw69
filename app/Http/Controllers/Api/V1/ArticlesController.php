<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Articles\ArticleStoreRequest;
use App\Http\Resources\ArticleResource;
use App\Models\Article;
use Illuminate\Http\Request;

class ArticlesController extends ApiV1Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $articles = Article::paginate(8);
        return ArticleResource::collection($articles);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(ArticleStoreRequest $request)
    {
        if (auth('sanctum')->check()) {
            $article = Article::create($request->validated());
            return new ArticleResource($article);
        }

        return 'Action unauthorized';
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        if (auth('sanctum')->check()) {
            $article = Article::find($id);
            if ($article) {
                $article->with('user');
                $article->makeVisible(['created_at', 'updated_at']);
                return new ArticleResource($article);
            }
            return response()->json([
                'data' => null,
                'message' => 'not found'
            ], 404);
        }
        return 'Action unauthorized';
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(ArticleStoreRequest $request, string $id)
    {
        if (auth('sanctum')->check()) {
            $article = Article::find($id);
            $article->update($request->all());
            return new ArticleResource($article);
        }
        return 'Action unauthorized';
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        if (auth('sanctum')->check()) {
            $article = Article::find($id);
            $article->delete();
            $articles = Article::paginate(8);
            return ArticleResource::collection($articles);
        }
        return 'Action unauthorized';
    }
}
