<?php

namespace Tests\Feature;

use App\Models\Article;
use App\Models\Comment;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class CommentsTest extends TestCase
{
    use DatabaseMigrations;
    private $user;
    private $article;
    private $comment;

    private string $url;
    private string $url_update;

    public const DATA = [
        'body' => 'body test',
        'user_id' => null,
        'article_id' => null
    ];

    public const DATA_Update = [
        'body' => 'body test',
        'user' => null,
        'article' => null
    ];

    protected function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->url = route('comments.store');
        $article = Article::factory()->create();
        $this->article = $article;
        $comment = Comment::factory()->create();
        $this->comment = $comment;
        $this->url_update = route('comments.update', compact('comment'));
    }

    /**
     * A basic feature test example.
     */
    public function test_example(): void
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function test_successful_creation_of_comment()
    {
        Sanctum::actingAs(
            User::factory()->create(), ['*']
        );
        $data = self::DATA;
        $data['user_id'] = $this->user->id;
        $data['article_id'] = $this->article->id;
        $this->assertDatabaseMissing('comments', $data);
        $response = $this->postJson($this->url, $data);
        $response->assertStatus(201);
        $response->assertJsonStructure(
            [
                'data' => [
                    'body', 'id', 'user', 'article'
                ]
            ]
        );

        $this->assertDatabaseHas('comments', $data);
    }

    public function test_trying_create_comment_unauthorized()
    {
        $data = self::DATA;
        $data['user_id'] = $this->user->id;
        $data['article_id'] = $this->article->id;
        $this->assertDatabaseMissing('comments', $data);
        $response = $this->postJson($this->url, $data);
        $response->assertStatus(401);
        $this->assertDatabaseMissing('comments', $data);
    }

    public function test_trying_to_create_comment_without_body()
    {
        Sanctum::actingAs(
            User::factory()->create(), ['*']
        );
        $data = self::DATA;
        $data['user_id'] = $this->user->id;
        $data['article_id'] = $this->article->id;
        $data['body'] = null;
        $this->assertDatabaseMissing('comments', $data);
        $response = $this->postJson($this->url, $data);
        $response->assertStatus(422);
        $response->assertJsonStructure(
            [
                'message',
                'errors' => [
                    'body'
                ]
            ]
        );
    }

    public function test_trying_to_create_comment_without_user()
    {
        Sanctum::actingAs(
            User::factory()->create(), ['*']
        );
        $data = self::DATA;
        $data['article_id'] = $this->article->id;
        $this->assertDatabaseMissing('comments', $data);
        $response = $this->postJson($this->url, $data);
        $response->assertStatus(422);
        $response->assertJsonStructure(
            [
                'message',
                'errors' => [
                    'user_id'
                ]
            ]
        );
    }

    public function test_trying_to_create_comment_for_no_article()
    {
        Sanctum::actingAs(
            User::factory()->create(), ['*']
        );
        $data = self::DATA;
        $data['user_id'] = $this->user->id;
        $this->assertDatabaseMissing('comments', $data);
        $response = $this->postJson($this->url, $data);
        $response->assertStatus(422);
        $response->assertJsonStructure(
            [
                'message',
                'errors' => [
                    'article_id'
                ]
            ]
        );
    }

    public function test_trying_co_create_comment_without_data()
    {
        Sanctum::actingAs(
            User::factory()->create(), ['*']
        );
        $data = [];
        $response = $this->postJson($this->url, $data);
        $response->assertStatus(422);
        $response->assertJsonStructure(
            [
                'message',
                'errors' => [
                    'user_id',
                    'article_id',
                    'body',
                ]
            ]
        );

    }

    public function test_trying_to_update_article():void
    {
        Sanctum::actingAs(
            User::factory()->create(), ['*']
        );
        $data = self::DATA_Update;
        $data['user_id'] = $this->user->id;
        $data['article_id'] = $this->article->id;
        $response = $this->putJson($this->url_update, $data);
        $response->assertStatus(200);
        $this->assertDatabaseHas('comments', [
            'body' => 'body test']);
    }

    public function test_trying_to_update_article_unauthorized():void
    {
        $data = self::DATA_Update;
        $data['user_id'] = $this->user->id;
        $data['article_id'] = $this->article->id;
        $response = $this->putJson($this->url_update, $data);
        $response->assertStatus(401);
        $this->assertDatabaseMissing('comments', [
            'body' => 'body test']);
    }

    public function test_trying_to_update_comment_without_user_id(): void
    {
        Sanctum::actingAs(
            User::factory()->create(), ['*']
        );
        $data = self::DATA_Update;
        $data['article_id'] = $this->article->id;
        $response = $this->putJson($this->url_update, $data);
        $response->assertStatus(422);
        $this->assertDatabaseMissing('articles', [
            'body' => 'body test', 'article_id' => '1']);
        $response->assertJsonStructure(
            [
                'message',
                'errors' => [
                    'user_id'
                ]
            ]
        );
    }

    public function test_trying_to_update_comment_without_article_id(): void
    {
        Sanctum::actingAs(
            User::factory()->create(), ['*']
        );
        $data = self::DATA_Update;
        $data['user_id'] = $this->user->id;
        $response = $this->putJson($this->url_update, $data);
        $response->assertStatus(422);
        $this->assertDatabaseMissing('articles', [
            'body' => 'body test', 'article_id' => '1']);
        $response->assertJsonStructure(
            [
                'message',
                'errors' => [
                    'article_id',
                ]
            ]
        );
    }

    public function test_trying_to_update_comment_without_data(): void
    {
        Sanctum::actingAs(
            User::factory()->create(), ['*']
        );
        $data = [];
        $response = $this->putJson($this->url_update, $data);
        $response->assertStatus(422);
        $this->assertDatabaseMissing('articles', [
            'body' => 'body test', 'article_id' => '1']);
        $response->assertJsonStructure(
            [
                'message',
                'errors' => [
                    'user_id',
                    'article_id',
                    'body',
                ]
            ]
        );
    }

    public function test_trying_to_delete_comment(): void
    {
        Sanctum::actingAs(
            User::factory()->create(), ['*']
        );
        $response = $this->deleteJson($this->url_update);
        $response->assertStatus(200);
        $this->assertDatabaseMissing('comments', [$this->comment->body]);
    }

    public function test_trying_to_delete_comment_unauthorized()
    {
        $response = $this->deleteJson($this->url_update);
        $response->assertStatus(401);
    }
}
