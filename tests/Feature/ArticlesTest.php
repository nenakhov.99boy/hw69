<?php

namespace Tests\Feature;

use App\Models\Article;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class ArticlesTest extends TestCase
{
    use DatabaseMigrations;

    private $user;
    private $article;

    private string $url;
    private string $url_update;

    public const DATA = [
        'title' => 'test',
        'body' => 'body test',
        'user_id' => null
    ];

    public const DATA_Update = [
        'title' => 'test',
        'body' => 'body test',
        'user_id' => null
    ];

    protected function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->url = route('articles.store');
        $article = Article::factory()->create();
        $this->article = $article;
        $this->url_update = route('articles.update', compact('article'));
    }

    /**
     * @group articles
     */
    public function test_success_create_article(): void
    {
        Sanctum::actingAs(
            User::factory()->create(), ['*']
        );
        $data = self::DATA;
        $data['user_id'] = $this->user->id;
        $this->assertDatabaseMissing('articles', $data);
        $response = $this->postJson($this->url, $data);
        $response->assertStatus(201);
        $response->assertJsonStructure(
            [
                'data' => [
                    'title', 'body', 'id', 'user'
                ]
            ]
        );

        $this->assertDatabaseHas('articles', $data);
    }

    public function test_trying_to_create_article_unauthorized()
    {
        $data = self::DATA;
        $data['user_id'] = $this->user->id;
        $response = $this->postJson($this->url, $data);
        $response->assertStatus(401);
    }

    public function test_view_article_index_unauthorized()
    {
        Sanctum::actingAs(
            User::factory()->create(), ['*']
        );
        $response = $this->getJson(route('articles.index'));
        $response->assertStatus(200);
    }

    public function test_view_article_index_authorized()
    {
        $response = $this->getJson(route('articles.index'));
        $response->assertStatus(200);
    }

    /**
     * @group articles
     */
    public function test_trying_create_article_without_title(): void
    {
        Sanctum::actingAs(
            User::factory()->create(), ['*']
        );
        $data = self::DATA;
        $data['user_id'] = $this->user->id;
        $data['title'] = null;
        $this->assertDatabaseMissing('articles', $data);
        $response = $this->postJson($this->url, $data);
        $response->assertStatus(422);
        $response->assertJsonStructure(
            [
                'message',
                'errors' => [
                    'title'
                ]
            ]
        );

        $this->assertDatabaseMissing('articles', $data);
    }

    /**
     * @group articles
     */
    public function test_trying_create_article_without_body(): void
    {
        Sanctum::actingAs(
            User::factory()->create(), ['*']
        );
        $data = self::DATA;
        $data['user_id'] = $this->user->id;
        $data['body'] = null;
        $this->assertDatabaseMissing('articles', $data);
        $response = $this->postJson($this->url, $data);
        $response->assertStatus(422);
        $response->assertJsonStructure(
            [
                'message',
                'errors' => [
                    'body'
                ]
            ]
        );

        $this->assertDatabaseMissing('articles', $data);
    }

    /**
     * @group articles
     */
    public function test_trying_create_article_without_user_id(): void
    {
        Sanctum::actingAs(
            User::factory()->create(), ['*']
        );
        $data = self::DATA;
        $this->assertDatabaseMissing('articles', $data);
        $response = $this->postJson($this->url, $data);
        $response->assertStatus(422);
        $response->assertJsonStructure(
            [
                'message',
                'errors' => [
                    'user_id'
                ]
            ]
        );

        $this->assertDatabaseMissing('articles', $data);
    }

    /**
     * @group articles
     */
    public function test_trying_create_article_without_all_data(): void
    {
        Sanctum::actingAs(
            User::factory()->create(), ['*']
        );
        $data = [];
        $response = $this->postJson($this->url, $data);
        $response->assertStatus(422);
        $response->assertJsonStructure(
            [
                'message',
                'errors' => [
                    'title',
                    'body',
                    'user_id'
                ]
            ]
        );
    }

    public function test_trying_to_update_article():void
    {
        Sanctum::actingAs(
            User::factory()->create(), ['*']
        );
        $data = self::DATA_Update;
        $data['user_id'] = $this->user->id;
        $response = $this->putJson($this->url_update, $data);
        $response->assertStatus(200);
        $this->assertDatabaseHas('articles', ['title' => 'test',
            'body' => 'body test',]);
    }

    public function test_trying_to_update_article_unauthorized()
    {
        $data = self::DATA_Update;
        $data['user_id'] = $this->user->id;
        $response = $this->putJson($this->url_update, $data);
        $response->assertStatus(401);
    }

    public function test_trying_to_update_article_without_user_id(): void
    {
        Sanctum::actingAs(
            User::factory()->create(), ['*']
        );
        $data = self::DATA_Update;
        $response = $this->putJson($this->url_update, $data);
        $response->assertStatus(422);
        $this->assertDatabaseMissing('articles', ['title' => 'test',
            'body' => 'body test',]);
    }

    public function test_trying_to_delete_article(): void
    {
        Sanctum::actingAs(
            User::factory()->create(), ['*']
        );
        $response = $this->deleteJson($this->url_update);
        $response->assertStatus(200);
        $this->assertDatabaseMissing('articles', [$this->article->body, $this->article->title]);
    }

    public function test_trying_to_delete_article_unauthorized()
    {
        $response = $this->deleteJson($this->url_update);
        $response->assertStatus(401);
    }

}
